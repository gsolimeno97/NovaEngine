#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <GL/glew.h>

class HelperFunctions
{
public:
	HelperFunctions();
	~HelperFunctions();

	static bool EndsWith(const char* string, const char* suffix);
	static bool FileExists(const char* filepath);
	static std::string LoadFile(std::string path);

	static unsigned int LoadBMP(const char* imagepath);

};

