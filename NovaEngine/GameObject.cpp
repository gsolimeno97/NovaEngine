#include "GameObject.h"

static uint32_t id = 0;


GameObject::GameObject()
{
	_transform = new Transform();
	id++;
}

GameObject::GameObject(GameObjectInfo& info)
{
	_transform = new Transform();
	if (info.modelInfo != NULL)
	{
		_renderable = new Renderable(*info.modelInfo);
	}
	_transform->SetPosition(info.position);
	_transform->SetRotation(info.rotation);
	_transform->SetScale(info.scale);
	id++;
}


GameObject::~GameObject()
{
}

void GameObject::LoadRenderableModel(std::string path)
{
	if (_renderable) delete _renderable;
	_renderable = new Renderable(path);
}

void GameObject::SetTransform(Transform * newTransform)
{

	_transform = newTransform;

}

void GameObject::Update()
{

	_renderable->Draw(Camera::mainCamera().GetViewMatrix(), Camera::mainCamera().GetProjectionMatrix(), _transform->GetTransformMatrix());

}

Transform * GameObject::GetTransform()
{
	return _transform;
}

Renderable * GameObject::GetRenderable()
{
	return _renderable;
}

uint32_t GameObject::ObjectID()
{
	return id;
}

std::string GameObject::ToString()
{
	return "";
}
