#pragma once

#include <GL/glew.h>
#include <iostream>
#include <vector>
#include "HelperFunctions.h"

enum SHADER_TYPE
{
	VERTEX_SHADER = GL_VERTEX_SHADER,
	FRAGMENT_SHADER = GL_FRAGMENT_SHADER,
	COMPUTE_SHADER = GL_COMPUTE_SHADER,
	GEOMETRY_SHADER = GL_GEOMETRY_SHADER
};

class Shader
{

private:
	std::string _shaderCode;
	GLuint _shaderID;

public:
	Shader();
	Shader(std::string path, SHADER_TYPE type);
	~Shader();
	
	GLuint GetShaderID();

	static GLuint CompileToProgram(Shader* shaders, uint32_t count);

};

