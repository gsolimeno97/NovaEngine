#pragma once

#include <GLFW/glfw3.h>
#include <iostream>

typedef enum KEYCODE
{
	KEY_0 = 48,
	KEY_1,
	KEY_2,
	KEY_3,
	KEY_4,
	KEY_5,
	KEY_6,
	KEY_7,
	KEY_8,
	KEY_9,
	KEY_A = 65,
	KEY_B,
	KEY_C,
	KEY_D,
	KEY_E,
	KEY_F,
	KEY_G,
	KEY_H,
	KEY_I,
	KEY_J,
	KEY_K,
	KEY_L,
	KEY_M,
	KEY_N,
	KEY_O,
	KEY_P,
	KEY_Q,
	KEY_R,
	KEY_S,
	KEY_T,
	KEY_U,
	KEY_V,
	KEY_W,
	KEY_X,
	KEY_Y,
	KEY_Z,
	KEY_APOSTROPHE = 39,
	KEY_BACKSLASH = 92,
	KEY_BACKSPACE = 259,
	KEY_CAPS_LOCK = 280,
	KEY_COMMA = 44,
	KEY_DELETE = 261,
	KEY_END = 269,
	KEY_ENTER = 257,
	KEY_EQUAL = 61,
	KEY_ESCAPE = 256,
	KEY_F1 = 290,
	KEY_F2,
	KEY_F3,
	KEY_F4,
	KEY_F5,
	KEY_F6,
	KEY_F7,
	KEY_F8,
	KEY_F9,
	KEY_F10,
	KEY_F11,
	KEY_F12,
	KEY_F13,
	KEY_F14,
	KEY_F15,
	KEY_F16,
	KEY_F17,
	KEY_F18,
	KEY_F19,
	KEY_F20,
	KEY_F21,
	KEY_F22,
	KEY_F23,
	KEY_F24,
	KEY_F25,
	KEY_GRAVE_ACCENT = 96,
	KEY_HOME = 268,
	KEY_INSERT = 260,
	KEY_N1 = 320,
	KEY_N2,
	KEY_N3,
	KEY_N4,
	KEY_N5,
	KEY_N6,
	KEY_N7,
	KEY_N8,
	KEY_N9,
	KEY_ADD = 334,
	KEY_DECIMAL = 330,
	KEY_DIVIDE = 331,
	KEY_NUM_ENTER = 335,
	KEY_NUM_EQUAL,
	KEY_NUM_MULT = 332,
	KEY_NUM_SUB = 333,
	KEY_LAST = 348,
	KEY_ALEFT = 342,
	KEY_LEFT_BRACKET = 91,
	KEY_RIGHT_BRACKET = 93,
	KEY_MENU = 348,
	KEY_MINUS = 45,
	KEY_NUM_LOCK = 282,
	KEY_PAGE_DOWN = 267,
	KEY_PAGE_UP = 266,
	KEY_PAUSE = 284,
	KEY_PERIOD = 46,
	KEY_PRINT_SCREEN = 283,
	KEY_RIGHT = 262,
	KEY_LEFT = 263,
	KEY_DOWN = 264,
	KEY_UP = 265,
	KEYL_LSHIFT = 340,
	KEY_RSHIFT = 344,
	KEY_LCTRL = 341,
	KEY_RCTRL = 345,
	KEY_ARIGHT = 346,
	KEY_SCROLL_LOCK = 281,
	KEY_SEMICOLON = 59,
	KEY_SLASH = 47,
	KEY_SPACE = 32,
	KEY_TAB = 258,

};

class InputManager
{
	friend class GameWindow;

protected:
	static void SetWindow(GLFWwindow* win);

private:
	InputManager();


public:
	double _mousex, _mousey;

	static InputManager* GetInstance();

	void UpdateInput();

	float GetMouseHorizontalAxis();

	float GetMouseVerticalAxis();

	bool GetKeyDown(KEYCODE key);
	bool GetKeyUp(KEYCODE key);

	static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
	~InputManager();
};

