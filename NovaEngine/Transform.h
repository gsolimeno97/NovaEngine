#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>
#include <iterator>

#include "IObject.h"

///TODO: Insert generics version of functions

class Transform : IObject
{
private:

	std::vector<Transform*>_childs;
	Transform* _parent;

	glm::vec3 _posVec3;
	glm::vec3 _scalVec3;

	glm::mat4 _position;
	glm::mat4 _rotation;
	glm::mat4 _scale;


public:
	Transform();
	~Transform();

	///GETTERS

	//Returns position in world space
	glm::vec3 GetPosition();

	//Returns scale of the transform
	glm::vec3 GetScale();

	//Returns rotation of the transform
	glm::vec3  GetRotation();

	//Returns transform matrix
	glm::mat4 GetTransformMatrix();

	//Returns the forward vector of the transform
	glm::vec3 GetForward();

	//Returns the right vector of the transform
	glm::vec3 GetRight();

	//Returns the up vector of the transform
	glm::vec3 GetUp();

	//Returns the class description in string form
	std::string ToString() override;

	//Returns the instance ID
	uint32_t ObjectID() override;

	///SETTERS

	//Sets position in world space
	template<class X, class Y, class Z >
	void SetPosition(X x, Y y, Z z);
	void SetPosition(glm::vec3 newPos);
	
	//Sets rotation in euler angles
	template<class X, class Y, class Z >
	void SetRotation(X x, Y y, Z z);
	void SetRotation(glm::vec3 newRot);

	//Sets scale 
	template<class X, class Y, class Z >
	void SetScale(X x, Y y, Z z);
	void SetScale(glm::vec3 newScale);

	//Sets this transform's parent
	void SetParent(Transform* parent);

	///UPDATERS

	//Moves in the world space
	void Translate(glm::vec3 translation);
	template<class X, class Y, class Z >
	void Translate(X x, Y y, Z z);

	//Rotates the transform using euler angles
	void Rotate(glm::vec3 rotation);
	template<class X, class Y, class Z >
	void Rotate(X x, Y y, Z z);

	//Scales the transform
	void Scale(glm::vec3 scale);
	template<class X, class Y, class Z >
	void Scale(X x, Y y, Z z);

	//Adds a child to the childs
	void AddChild(Transform* child);

};

///TODO: UPDATE THIS SHIT

template<class X, class Y, class Z>
inline void Transform::SetPosition(X x, Y y, Z z)
{
	_position = glm::vec3(x, y, z);
	UpdateTransformMatrix();
}

template<class X, class Y, class Z>
inline void Transform::SetRotation(X x, Y y, Z z)
{

	_rotation = glm::vec3(x, y, z);
	UpdateTransformMatrix();

}

template<class X, class Y, class Z>
inline void Transform::SetScale(X x, Y y, Z z)
{

	_scale = glm::vec3(x, y, z);
	UpdateTransformMatrix();

}

template<class X, class Y, class Z >
inline void Transform::Translate(X x, Y y, Z z)
{
	_posVec3 += glm::vec3(x,y,z);
	_position = glm::translate(_position, glm::vec3(x, y, z));

	for (std::vector<Transform*>::iterator it = _childs.begin(); it != _childs.end(); it++)
	{
		Transform* t = *it;
		t->Translate(glm::vec3(x, y, z));
	}

}

template<class X, class Y, class Z >
inline void Transform::Rotate(X x, Y y, Z z)
{

	_rotation = glm::rotate(_rotation, x, glm::vec3(1, 0, 0));
	_rotation = glm::rotate(_rotation, y, glm::vec3(0, 1, 0));
	_rotation = glm::rotate(_rotation, z, glm::vec3(0, 0, 1));

	for (std::vector<Transform*>::iterator it = _childs.begin(); it != _childs.end(); it++)
	{
		Transform* t = *it;
		t->_rotation = glm::rotate(t->_rotation, x, glm::vec3(1, 0, 0));
		t->_rotation = glm::rotate(t->_rotation, y, glm::vec3(0, 1, 0));
		t->_rotation = glm::rotate(t->_rotation, z, glm::vec3(0, 0, 1));
	}

}

template<class X, class Y, class Z >
inline void Transform::Scale(X x, Y y, Z z)
{
	_scalVec3 += glm::vec3(x, y, z);
	_scale = glm::scale(_scale, glm::vec3(x,y,z));
	for (std::vector<Transform*>::iterator it = _childs.begin(); it != _childs.end(); it++)
	{
		Transform* t = *it;
		t->_scale = glm::scale(_scale, glm::vec3(x, y, z));
	}
}
