#pragma once
#include "IObject.h"
#include "Transform.h"
#include "Camera.h"
#include "Renderable.h"

#include <vector>

struct GameObjectInfo
{
	RenderableInfo* modelInfo = NULL;
	glm::vec3 position;
	glm::vec3 rotation;
	glm::vec3 scale;
};

class GameObject :
	public IObject
{

private:
	std::string _name;

	Transform* _transform;
	Renderable* _renderable = NULL;
public:
	GameObject();
	GameObject(GameObjectInfo& info);
	~GameObject();

	void LoadRenderableModel(std::string path);

	void SetTransform(Transform* newTransform);

	void Update();

	Transform* GetTransform();
	Renderable* GetRenderable();

	uint32_t ObjectID() override;
	std::string ToString() override;

};

