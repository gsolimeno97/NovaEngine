#include "Time.h"
	
static GLdouble _totalTime;
static GLfloat _deltaTime;
static GLdouble _lastTime;

void Time::InitTime()
{

	_totalTime = 0.0f;
	_lastTime = 0.0f;

}

void Time::UpdateTime1()
{

	_totalTime = glfwGetTime();

}

void Time::UpdateTime2()
{

	_deltaTime = _totalTime - _lastTime;
	_lastTime = _totalTime;
}

GLfloat Time::GetDeltaTime()
{
	return _deltaTime;
}

GLfloat Time::GetTotalTime()
{
	return _totalTime;
}
