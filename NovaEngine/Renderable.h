#pragma once

#include <GL/glew.h>
#include <assimp/scene.h>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>

#include "IObject.h"
#include "Image.h"
#include "Shader.h"

struct RenderableInfo
{
	std::string modelFile;
	GLuint renderingProgram;
	GLuint texture;
};

class Renderable :
	public IObject
{
private:

	void LoadModel(std::string path);

	GLuint _vao, _vbo, _uvo, _ivo;
	GLuint _program = -1;
	GLuint _texture = -1;
	GLuint mvpLoc, samplPos;
	Assimp::Importer importer;

	std::vector<float> verts;
	std::vector<float> uvs;
	std::vector<float> norms;
	std::vector<unsigned int>indices;
public:
	void Draw(glm::mat4 viewMat, glm::mat4 proj, glm::mat4 modMat);

	void SetTexture(GLuint tex);
	void SetRenderingProgram(GLuint program);

	uint32_t ObjectID() override;
	std::string ToString() override;

	Renderable(std::string filePath);
	Renderable(RenderableInfo& info);
	~Renderable();
};

