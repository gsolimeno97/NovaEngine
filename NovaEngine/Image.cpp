#include "Image.h"

static uint32_t id;

Image::Image(const char * path)
{

	if (HelperFunctions::FileExists(path))
	{

		FIBITMAP* temp;
		FREE_IMAGE_FORMAT format = FREE_IMAGE_FORMAT::FIF_UNKNOWN;

		if (HelperFunctions::EndsWith(path, "bmp"))
		{
			format = FREE_IMAGE_FORMAT::FIF_BMP;
		}
		else if (HelperFunctions::EndsWith(path, "png"))
		{
			format = FREE_IMAGE_FORMAT::FIF_PNG;
		}
		else if (HelperFunctions::EndsWith(path, "jpeg"))
		{
			format = FREE_IMAGE_FORMAT::FIF_JPEG;

		}
		else if (HelperFunctions::EndsWith(path, "jpg"))
		{
			format = FREE_IMAGE_FORMAT::FIF_JPEG;
		}
		else if (HelperFunctions::EndsWith(path, "tga"))
		{
			format = FREE_IMAGE_FORMAT::FIF_TARGA;
		}
		else if (HelperFunctions::EndsWith(path, "dds"))
		{
			format = FREE_IMAGE_FORMAT::FIF_DDS;
		}

		if (format != FREE_IMAGE_FORMAT::FIF_UNKNOWN)
		{
			temp = FreeImage_Load(format, path, 0);

			_bpp = FreeImage_GetBPP(temp);

			if (_bpp == 32)
			{
				bitmap = temp;
			}
			else
			{
				bitmap = FreeImage_ConvertTo32Bits(temp);
			}

			_width = FreeImage_GetWidth(bitmap);
			_height = FreeImage_GetHeight(bitmap);

			glGenTextures(1, &_texID);

			glBindTexture(GL_TEXTURE_2D, _texID);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, _width, _height, 0, GL_BGRA, GL_UNSIGNED_BYTE, GetData());

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		}
		else
		{
			std::cout << "Could not open image\n";
			Image();
		}
	}
	else
	{
		std::cout << "File does not exists! " << path << "\n";
		Image();
	}

	id++;
}

Image::Image()
{

	FreeImage_Allocate(800, 800, 32);

	id++;
}

uint32_t Image::GetWidth()
{
	return _width;
}

uint32_t Image::GetHeight()
{
	return _height;
}

uint32_t Image::GetBytesPerPixel()
{
	return _bpp;
}

GLubyte* Image::GetData()
{
		return FreeImage_GetBits(bitmap);
}

GLuint Image::GetTextureID()
{
	return _texID;
}

std::string Image::ToString()
{
	std::stringstream stri;
	stri << "Image ID: " << ObjectID() << std::endl;
	stri << "Image width: " << ObjectID() << std::endl;
	stri << "Image height: " << ObjectID() << std::endl;
	stri << "Image BPP: " << ObjectID() << std::endl;
	return stri.str();
}


uint32_t Image::ObjectID()
{
	return id;
}

Image::~Image()
{

	/*if (bitmap)
	{
		FreeImage_Unload(bitmap);
	}*/

}
