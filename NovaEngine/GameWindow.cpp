#include "GameWindow.h"

static uint32_t id;

static bool _init = false;

static GLFWwindow * _window = nullptr;

void GameWindow::InitWindow()
{
	if (_window)
	{
		glfwDestroyWindow(_window);
	}
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_SAMPLES, _aa);

	_window = glfwCreateWindow(_width, _height, _title.c_str(), _isFullscreen == true ? glfwGetPrimaryMonitor() : NULL, NULL);

	glfwSetInputMode(_window, GLFW_STICKY_KEYS, GLFW_TRUE);
	glfwSetInputMode(_window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

	glfwMakeContextCurrent(_window);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
}

GameWindow::GameWindow(std::string title, int width, int height, int xpos, int ypos, bool fullscreen)
{

	if (!_init)
	{
		if (!glfwInit())
		{
			std::cout << "Cannot initialize GLFW! " << std::endl;
			std::cin.get();
			std::exit(-1);
		}
	}

	_title = title;
	_width = width;
	_height = height;
	_xpos = xpos;
	_ypos = ypos;
	_isFullscreen = fullscreen;
	_isRunning = true;

	InitWindow();

	if (!_init)
	{
		glewExperimental = true;
		if (glewInit())
		{
			std::cout << "Cannot initialize GLEW! " << std::endl;
			std::cin.get();
			//std::exit(-1);
		}


		_init = true;
	}

	InputManager::SetWindow(_window);

	id++;

}

void GameWindow::swapBuffers()
{
	
	glfwSwapBuffers(_window);

}

GameWindow::~GameWindow()
{
	glfwDestroyWindow(_window);
}

void GameWindow::SetAALevel(AA_LEVEL naalev)
{

	_aa = naalev;
	InitWindow();

}

std::string GameWindow::ToString()
{
	std::stringstream stri;
	std::string isf = _isFullscreen == true ? "is fullscreen" : "is not fullscreen";

	stri	<< "Game window ID: "				<< id		<< std::endl
			<< "Game window title: "			<< _title	<< std::endl
			<< "Game window width: "			<< _width	<< std::endl
			<< "Game window height: "			<< _height	<< std::endl
			<< "Game window xpos: "				<< _xpos	<< std::endl
			<< "Game window ypos: "				<< _ypos	<< std::endl
			<< "Game window  "					<< isf		<< std::endl
			<< "Game window anti aliasing: "	<< _aa		<< std::endl;
	
	return std::string();
}

uint32_t GameWindow::ObjectID()
{
	return id;
}

bool GameWindow::isRunning()
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glCullFace(GL_BACK);
	return _isRunning;
}

void GameWindow::close()
{

	_isRunning = false;

}

int GameWindow::GetWidth()
{
	return _width;
}

int GameWindow::GetHeight()
{
	return _height;
}

int GameWindow::GetXpos()
{
	return _xpos;
}

int GameWindow::GetYPos()
{
	return _ypos;
}

AA_LEVEL GameWindow::GetAntialiasing()
{
	return _aa;
}

void GameWindow::SetWidth(int nwidth)
{

	_width = nwidth;
	glfwSetWindowSize(_window, _width, _height);
}

void GameWindow::SetHeight(int nheight)
{

	_height = nheight;
	glfwSetWindowSize(_window, _width, _height);
}

void GameWindow::SetXPos(int nxpos)
{

	_xpos = nxpos;
	glfwSetWindowPos(_window, _xpos, _ypos);

}

void GameWindow::SetYPos(int nypos)
{

	_ypos = nypos;
	glfwSetWindowPos(_window, _xpos, _ypos);

}


