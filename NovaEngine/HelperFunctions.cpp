#include "HelperFunctions.h"



HelperFunctions::HelperFunctions()
{
}


HelperFunctions::~HelperFunctions()
{
}

bool HelperFunctions::EndsWith(const char * string, const char * suffix)
{
	size_t strsize = strlen(string);
	size_t sfxsize = strlen(suffix);

	if (strsize < sfxsize) return false;

	int diff = strsize - sfxsize;
	const char* buf = string + diff;
	if (strcmp(buf, suffix) == 0)
	{
		return true;
	}

	return false;

}

bool HelperFunctions::FileExists(const char * filepath)
{
	std::ifstream infile(filepath);
	return infile.good();
}

std::string HelperFunctions::LoadFile(std::string path)
{

	std::ifstream file(path);
	std::string line = "";
	std::string ret = "";
	while (std::getline(file, line))
	{
		ret += (line + '\n');
	}
	return ret;
}

unsigned int HelperFunctions::LoadBMP(const char * imagepath)
{
	unsigned char header[54]; // Each BMP file begins by a 54-bytes header
	unsigned int dataPos;     // Position in the file where the actual data begins
	unsigned int width, height;
	unsigned int imageSize;   // = width*height*3
							  // Actual RGB data
	unsigned char * data;
	FILE * file = fopen(imagepath, "rb");
	if (!file) { printf("Image could not be opened\n"); return 0; }
	if (fread(header, 1, 54, file) != 54) { // If not 54 bytes read : problem
		printf("Not a correct BMP file\n");
		return false;
	}

	if (header[0] != 'B' || header[1] != 'M') {
		printf("Not a correct BMP file\n");
		return 0;
	}
	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	width = *(int*)&(header[0x12]);
	height = *(int*)&(header[0x16]);

	if (imageSize == 0)    imageSize = width*height * 3; // 3 : one byte for each Red, Green and Blue component
	if (dataPos == 0)      dataPos = 54; // The BMP header is done that way
										 // Create a buffer
	data = new unsigned char[imageSize];

	// Read the actual data from the file into the buffer
	fread(data, 1, imageSize, file);

	//Everything is in memory now, the file can be closed
	fclose(file);

	GLuint textureID;
	glGenTextures(1, &textureID);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);

	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

}
