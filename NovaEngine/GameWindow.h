#pragma once

#define GLEW_STATIC

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>

#include "IObject.h"
#include "InputManager.h"

typedef enum AA_LEVEL
{
	NO_ALIASING = 0,
	AA_2 = 2,
	AA_4 = 4,
	AA_8 = 8,
	AA_16 = 16
};

class GameWindow : IObject
{

private:
	std::string _title;
	int _width, _height, _xpos, _ypos;
	bool _isFullscreen;
	bool _isRunning;
	AA_LEVEL _aa = AA_4;


	void InitWindow();

public:

	GameWindow::GameWindow(std::string title, int width, int height, int xpos, int ypos, bool fullscreen);

	//Swaps buffers 
	void swapBuffers();

	//Check if window is still running
	bool isRunning();

	//Stops the rendering process for the window
	void close();

	//Gets the window width
	int GetWidth();

	//Gets the window height
	int GetHeight();

	//Gets the window xpos
	int GetXpos();

	//Gets the window ypos
	int GetYPos();

	//Gets the window antialiasing level
	AA_LEVEL GetAntialiasing();

	//Sets the window width
	void SetWidth(int nwidth);

	//Sets the window height
	void SetHeight(int nheight);

	//Sets the window x position
	void SetXPos(int nxpos);

	//Sets the window y position
	void SetYPos(int nypos);

	//Sets the antialiasing level
	void SetAALevel(AA_LEVEL naalev);

	std::string ToString() override;

	uint32_t ObjectID() override;

	~GameWindow();
};

