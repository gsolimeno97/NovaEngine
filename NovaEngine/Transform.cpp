#include "Transform.h"

static uint32_t id = 0;

Transform::Transform()
{

	SetPosition(glm::vec3(0, 0, 0));
	SetRotation(glm::vec3(0, 0, 0));
	SetScale(glm::vec3(1, 1, 1));
	id++;

}


Transform::~Transform()
{
}

glm::vec3 Transform::GetPosition()
{
	return _posVec3;
}

glm::vec3 Transform::GetScale()
{
	return _scalVec3;
}

glm::vec3 Transform::GetRotation()
{
	glm::vec4 v = glm::vec4(1, 1, 1, 0) *  _rotation;
	return glm::vec3(v.x, v.y, v.z);
}

glm::mat4 Transform::GetTransformMatrix()
{
	return _position * _rotation * _scale;
}

glm::vec3 Transform::GetForward()
{
	return glm::vec3(_rotation[0][2], _rotation[1][2], _rotation[2][2]);
}

glm::vec3 Transform::GetRight()
{
	return -glm::vec3(_rotation[0][0], _rotation[1][0], _rotation[2][0]);
}

glm::vec3 Transform::GetUp()
{
	return glm::vec3(_rotation[0][1], _rotation[1][1], _rotation[2][1]);
}

std::string Transform::ToString()
{
	std::stringstream str;
	str << "Transform ID: " << ObjectID() << std::endl;
	str << "Position X: " << GetPosition().x << " Y: " << GetPosition().y << " Z: " << GetPosition().z << std::endl;
	str << "Rotation X: " << GetRotation().x << " Y: " << GetRotation().y << " Z: " << GetRotation().z << std::endl;
	str << "Scale X: " << GetScale().x << " Y: " << GetScale().y << " Z: " << GetScale().z << std::endl;
	return str.str();
}

uint32_t Transform::ObjectID()
{
	return id;
}



void Transform::SetPosition(glm::vec3 newPos)
{
	_posVec3 = newPos;
	_position = glm::translate(glm::mat4(1) , newPos);
}

void Transform::SetRotation(glm::vec3 newRot)
{

	_rotation = glm::mat4(1);

	_rotation = glm::rotate(_rotation, newRot.x, glm::vec3(1, 0, 0));
	_rotation = glm::rotate(_rotation, newRot.y, glm::vec3(0, 1, 0));
	_rotation = glm::rotate(_rotation, newRot.z, glm::vec3(0, 0, 1));
}

void Transform::SetScale(glm::vec3 newScale)
{
	_scalVec3 = newScale;
	_scale = glm::scale(glm::mat4(1), newScale);

	for (std::vector<Transform*>::iterator it = _childs.begin(); it != _childs.end(); it++)
	{
		Transform* t = *it;
		t->SetScale(newScale);
	}

}

void Transform::SetParent(Transform * parent)
{

	_parent = parent;
	parent->AddChild(this);

}

void Transform::Translate(glm::vec3 translation)
{
	_posVec3 += translation;
	_position = glm::translate(_position, translation);

	for (std::vector<Transform*>::iterator it = _childs.begin(); it != _childs.end(); it++)
	{
		Transform* t = *it;
		t->Translate(translation);
	}

}

void Transform::Rotate(glm::vec3 rotation)
{
	_rotation = glm::rotate(_rotation, rotation.x, glm::vec3(1, 0, 0));
	_rotation = glm::rotate(_rotation, rotation.y, glm::vec3(0, 1, 0));
	_rotation = glm::rotate(_rotation, rotation.z, glm::vec3(0, 0, 1));

	for (std::vector<Transform*>::iterator it = _childs.begin(); it != _childs.end(); it++)
	{
		Transform* t = *it;
		t->_rotation = glm::rotate(t->_rotation, rotation.x, glm::vec3(1, 0, 0));
		t->_rotation = glm::rotate(t->_rotation, rotation.y, glm::vec3(0, 1, 0));
		t->_rotation = glm::rotate(t->_rotation, rotation.z, glm::vec3(0, 0, 1));
	}

}

void Transform::Scale(glm::vec3 scale)
{
	_scalVec3 += scale;
	_scale = glm::scale(_scale, scale);
	for (std::vector<Transform*>::iterator it = _childs.begin(); it != _childs.end(); it++)
	{
		Transform* t = *it;
		t->_scale = glm::scale(_scale, scale);
	}
}

void Transform::AddChild(Transform * child)
{

	_childs.push_back(child);

}

