#include "Camera.h"

static Camera* _actual = NULL;
static uint32_t id = 0;

Camera::Camera()
{
	if (_actual == NULL)
	{
		_actual = this;
	}
	_fov = 40.0f;
	_nearPane = 0.1f;
	_farPane = 100.0f;
	_renderWidth = 800;
	_renderHeight = 600;

	_transform = new Transform();
	
	id++;

}


Camera::~Camera()
{
}

Camera Camera::mainCamera()
{
	if (_actual == NULL)
	{
		_actual = new Camera();
	}
	return *_actual;
}

void Camera::SetMainCamera(Camera * newCamera)
{

	_actual = newCamera;

}

void Camera::SetFOV(float fov)
{

	_fov = fov;

}

void Camera::SetNearClippingPane(float npane)
{

	_nearPane = npane;

}

void Camera::SetFarClippingPane(float fpane)
{

	_farPane = fpane;

}

void Camera::SetRenderingWidth(int width)
{

	_renderWidth = width;

}

void Camera::SetRenderingHeight(int height)
{

	_renderHeight = height;

}

void Camera::SetTransform(Transform* newTrans)
{

	_transform = newTrans;

}

std::string Camera::ToString()
{
	std::stringstream str;

	str << "Camera ID: " << ObjectID() << std::endl;
	str << "Camera FOV " << _fov << std::endl;
	str << "Camera near plane " << _nearPane << std::endl;
	str << "Camera far plane " << _farPane << std::endl;
	str << "Camera render width " << _renderWidth << std::endl;
	str << "Camera render height " << _renderHeight << std::endl;

	return str.str();
}

uint32_t Camera::ObjectID()
{
	return id;
}

glm::mat4 Camera::GetProjectionMatrix()
{
	glm::mat4 _vm;
	_vm = glm::perspective(glm::radians(_fov), float(_renderWidth / _renderHeight), _nearPane, _farPane);
	return _vm;
}

glm::mat4 Camera::GetViewMatrix()
{
	return glm::lookAt(

		_transform->GetPosition(),
		_transform->GetPosition() + _transform->GetForward(),
		_transform->GetUp()
	);
}

float Camera::GetFOV()
{
	return _fov;
}

float Camera::GetNearPane()
{
	return _nearPane;
}

float Camera::GetFarPane()
{
	return _farPane;
}

float Camera::GetAspectRatio()
{
	return float(_renderWidth / _renderHeight);
}

int Camera::GetRenderingWidth()
{
	return _renderWidth;
}

int Camera::GetRenderingHeight()
{
	return _renderHeight;
}

Transform* Camera::GetTransform()
{
	return _transform;
}
