#include "InputManager.h"

static InputManager* _instance = nullptr;
static GLFWwindow* _window = nullptr;

void InputManager::SetWindow(GLFWwindow * win)
{

	_window = win;
	glfwSetKeyCallback(_window, KeyCallback);

}

InputManager::InputManager()
{


}


InputManager * InputManager::GetInstance()
{
	 if (_instance == nullptr)
	{
		_instance = new InputManager();
	}
	 return _instance;
}

void InputManager::UpdateInput()
{

	int _w, _h;
	glfwGetWindowSize(_window, &_w, &_h);
	glfwGetCursorPos(_window, &_mousex, &_mousey);
	glfwSetCursorPos(_window, _w / 2, _h / 2);

	glfwPollEvents();
}

float InputManager::GetMouseHorizontalAxis()
{
	int _w, _h;
	glfwGetWindowSize(_window, &_w, &_h);
	float val = ((_w / 2) / _mousex) - 1.0f;
	return  val;
}

float InputManager::GetMouseVerticalAxis()
{
	int _w, _h;
	glfwGetWindowSize(_window, &_w, &_h);
	return ((_h / 2) / _mousey) - 1.0f;
}



bool InputManager::GetKeyDown(KEYCODE key)
{
	int state = glfwGetKey(_window, key);
	if ( state == GLFW_PRESS)
	{
		return true;
	}
	return false;
}

bool InputManager::GetKeyUp(KEYCODE key)
{
	if (glfwGetKey(_window, key) == GLFW_RELEASE)
	{
		return true;
	}
	return false;
}

void InputManager::KeyCallback(GLFWwindow * window, int key, int scancode, int action, int mods)
{

	//std::cout << "Pressed key: " << key << std::endl;

}

InputManager::~InputManager()
{
}
