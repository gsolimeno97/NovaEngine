#pragma once

#include <GLFW/glfw3.h>

class Time
{
public:

	static void InitTime();

	//Sets the global time variable, must be done at the beginning of the game loop
	static void UpdateTime1();

	//Sets the delta time variable, must be done at the end of the game loop
	static void UpdateTime2();

	//Returns the delta time
	static GLfloat GetDeltaTime();

	//Returns the global time
	static GLfloat GetTotalTime();
};

