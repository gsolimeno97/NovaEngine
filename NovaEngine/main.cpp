// NovaEngine.cpp : Defines the entry point for the console application.
//


#include <iostream>
#include <btBulletDynamicsCommon.h>
#include <random>

#include "GameWindow.h"
#include "Camera.h"
#include "TestCube.h"
#include "GameObject.h"
#include "InputManager.h"
#include "Time.h"

int main()
{

	std::random_device rd;
	std::mt19937 rng(rd());
	std::uniform_int_distribution<int> uni(0, 20);

	FreeImage_Initialise(TRUE);
	GameWindow w("Hello, world!", 1024, 720, 0, 0, false);

	
	Camera::mainCamera().SetRenderingWidth(w.GetWidth());
	Camera::mainCamera().SetRenderingHeight(w.GetHeight());
	Camera::mainCamera().SetFOV(60.0f);
	Camera::mainCamera().GetTransform()->SetPosition(glm::vec3(0, 0, -10));
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

	Image im = Image("./texture.png");
	Shader shaders[2];

	shaders[0] = Shader("./shaders/defaultVertex.vs", VERTEX_SHADER);
	shaders[1] = Shader("./shaders/defaultFragment.fs", FRAGMENT_SHADER);

	//GameObject* tc[10] = {NULL};
	GLuint program = Shader::CompileToProgram(shaders, 2);

	RenderableInfo info;
	info.modelFile = "./cube.fbx";
	info.renderingProgram = program;
	info.texture = 	im.GetTextureID();

	GameObjectInfo GOinfo;
	GOinfo.modelInfo = &info;
	GOinfo.scale = glm::vec3(1, 1, 1);

	GameObject def;
	GameObject def2;
	def.LoadRenderableModel("./cube.fbx");
	def.GetRenderable()->SetRenderingProgram(program);
	def.GetRenderable()->SetTexture(program);
	def.GetTransform()->SetPosition(glm::vec3(0,0,0));
	def.GetTransform()->SetRotation(glm::vec3(0,0,0));
	
	def2.LoadRenderableModel("./cube.fbx");
	def2.GetRenderable()->SetRenderingProgram(program);
	def2.GetRenderable()->SetTexture(program);
	def2.GetTransform()->SetPosition(glm::vec3(0,-5,-2));
	def2.GetTransform()->SetRotation(glm::vec3(0,0,0));

	def.GetTransform()->AddChild(def2.GetTransform());
	float mover = 5.0f;
	float max = 10.0f;
	float diff = 0.05f;
	GameObject* tc[10];

	for (int i = 0; i < 10; i++)
	{
		GOinfo.position = glm::vec3(uni(rng), uni(rng), uni(rng));
		GOinfo.rotation = glm::vec3(uni(rng), uni(rng), uni(rng));
		tc[i] = new GameObject(GOinfo);
	}

	/**btBroadphaseInterface* broadphase = new btDbvtBroadphase();
	btDefaultCollisionConfiguration* collisionConfig = new btDefaultCollisionConfiguration();
	btCollisionDispatcher* dispatcher = new btCollisionDispatcher(collisionConfig);
	btSequentialImpulseConstraintSolver* solver = new btSequentialImpulseConstraintSolver();
	btDiscreteDynamicsWorld* world = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfig);

	world->setGravity(btVector3(0, -10, 0));

	btCollisionShape* gshape = new btStaticPlaneShape(btVector3(0, 1, 0), 1);
	btCollisionShape* fshape = new btSphereShape(1);

	btDefaultMotionState* gms = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, -1, 0)));
	btRigidBody::btRigidBodyConstructionInfo grbci(0, gms, gshape, btVector3(0, 0, 0));
	btRigidBody* gBody = new btRigidBody(grbci);

	world->addRigidBody(gBody);

	btDefaultMotionState* fms = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 50, 0)));
	btScalar mass = 1;
	btVector3 fallInertia(0, 0, 0);
	fshape->calculateLocalInertia(mass, fallInertia);
	btRigidBody::btRigidBodyConstructionInfo frbci(mass, fms, fshape, fallInertia);
	btRigidBody* fBody = new btRigidBody(frbci);
	world->addRigidBody(fBody);*/

	float x = 0;
	float y = 0;
	while (w.isRunning())
	{
		InputManager::GetInstance()->UpdateInput();
		Time::UpdateTime1();

		mover += diff;

		if (mover > max || mover < -max)
		{
			diff = -diff;
		}

		def.GetTransform()->Translate(glm::vec3(0, diff, 0));

		if (InputManager::GetInstance()->GetKeyDown(KEY_ESCAPE))
		{
			w.close();
		}
		if (InputManager::GetInstance()->GetKeyDown(KEY_W))
		{
			Camera::mainCamera().GetTransform()->Translate(Camera::mainCamera().GetTransform()->GetForward() * 7.0f * Time::GetDeltaTime());
		}
		if (InputManager::GetInstance()->GetKeyDown(KEY_S))
		{
			Camera::mainCamera().GetTransform()->Translate(-Camera::mainCamera().GetTransform()->GetForward() * 7.0f * Time::GetDeltaTime());
		}
		
		if (InputManager::GetInstance()->GetKeyDown(KEY_D))
		{
			Camera::mainCamera().GetTransform()->Translate(Camera::mainCamera().GetTransform()->GetRight() * 7.0f * Time::GetDeltaTime());
		}
		if (InputManager::GetInstance()->GetKeyDown(KEY_A))
		{
			Camera::mainCamera().GetTransform()->Translate(-Camera::mainCamera().GetTransform()->GetRight() * 7.0f * Time::GetDeltaTime());
		}

		if (InputManager::GetInstance()->GetKeyDown(KEY_Q))
		{
			Camera::mainCamera().GetTransform()->Translate(Camera::mainCamera().GetTransform()->GetUp() * 7.0f * Time::GetDeltaTime());
		}
		if (InputManager::GetInstance()->GetKeyDown(KEY_E))
		{
			Camera::mainCamera().GetTransform()->Translate(-Camera::mainCamera().GetTransform()->GetUp() * 7.0f * Time::GetDeltaTime());
		}

		float h = InputManager::GetInstance()->GetMouseHorizontalAxis();
		float v = InputManager::GetInstance()->GetMouseVerticalAxis();
		if (h != 0 || v != 0)
		{

			y += v;
			if (y > glm::radians(40.0f)) y = glm::radians(40.0f);
			if (y < glm::radians(-40.0f)) y = glm::radians(-40.0f);

			x += h;
		}

		Camera::mainCamera().GetTransform()->SetRotation(glm::vec3(y, -x, 0));

		for (int i = 0; i < 10; i++)
		{
			tc[i]->Update();
		}

		def.Update();
		def2.Update();
		w.swapBuffers();
		glfwPollEvents();

		std::cout	<< "Delta time: "		<< Time::GetDeltaTime()		 << std::endl
							<< "Total time: "		<< Time::GetTotalTime()		 << std::endl
							<< std::endl;

		/*world->stepSimulation(1 / 60.f, 10);
		btTransform trans;
		fBody->getMotionState()->getWorldTransform(trans);*/
		Time::UpdateTime2();
	}

	/*delete world;
	delete solver;
	delete dispatcher;
	delete collisionConfig;
	delete broadphase;*/
	return 0;


}

