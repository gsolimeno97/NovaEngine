#include "TestCube.h"



void TestCube::Init(bool useHardcoded)
{
	_transform = new Transform();

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glGenBuffers(1, &uvo);
	glGenBuffers(1, &ivo);

	/*const GLfloat vertex_data[] = {
		-1.0f,-1.0f,-1.0f, // triangle 1 : begin
		-1.0f,-1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f, // triangle 1 : end
		1.0f, 1.0f,-1.0f, // triangle 2 : begin
		-1.0f,-1.0f,-1.0f,
		-1.0f, 1.0f,-1.0f, // triangle 2 : end
		1.0f,-1.0f, 1.0f,
		-1.0f,-1.0f,-1.0f,
		1.0f,-1.0f,-1.0f,
		1.0f, 1.0f,-1.0f,
		1.0f,-1.0f,-1.0f,
		-1.0f,-1.0f,-1.0f,
		-1.0f,-1.0f,-1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f,-1.0f,
		1.0f,-1.0f, 1.0f,
		-1.0f,-1.0f, 1.0f,
		-1.0f,-1.0f,-1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f,-1.0f, 1.0f,
		1.0f,-1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f,-1.0f,-1.0f,
		1.0f, 1.0f,-1.0f,
		1.0f,-1.0f,-1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f,-1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f,-1.0f,
		-1.0f, 1.0f,-1.0f,
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f,-1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f,-1.0f, 1.0f
	};

	static GLfloat uv_data[] = {
		0.000059f, 1.0f - 0.000004f,
		0.000103f, 1.0f - 0.336048f,
		0.335973f, 1.0f - 0.335903f,
		1.000023f, 1.0f - 0.000013f,
		0.667979f, 1.0f - 0.335851f,
		0.999958f, 1.0f - 0.336064f,
		0.667979f, 1.0f - 0.335851f,
		0.336024f, 1.0f - 0.671877f,
		0.667969f, 1.0f - 0.671889f,
		1.000023f, 1.0f - 0.000013f,
		0.668104f, 1.0f - 0.000013f,
		0.667979f, 1.0f - 0.335851f,
		0.000059f, 1.0f - 0.000004f,
		0.335973f, 1.0f - 0.335903f,
		0.336098f, 1.0f - 0.000071f,
		0.667979f, 1.0f - 0.335851f,
		0.335973f, 1.0f - 0.335903f,
		0.336024f, 1.0f - 0.671877f,
		1.000004f, 1.0f - 0.671847f,
		0.999958f, 1.0f - 0.336064f,
		0.667979f, 1.0f - 0.335851f,
		0.668104f, 1.0f - 0.000013f,
		0.335973f, 1.0f - 0.335903f,
		0.667979f, 1.0f - 0.335851f,
		0.335973f, 1.0f - 0.335903f,
		0.668104f, 1.0f - 0.000013f,
		0.336098f, 1.0f - 0.000071f,
		0.000103f, 1.0f - 0.336048f,
		0.000004f, 1.0f - 0.671870f,
		0.336024f, 1.0f - 0.671877f,
		0.000103f, 1.0f - 0.336048f,
		0.336024f, 1.0f - 0.671877f,
		0.335973f, 1.0f - 0.335903f,
		0.667969f, 1.0f - 0.671889f,
		1.000004f, 1.0f - 0.671847f,
		0.667979f, 1.0f - 0.335851f
	};

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data), vertex_data, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, uvo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(uv_data), uv_data, GL_STATIC_DRAW);*/

	const  aiScene* scene = importer.ReadFile("./cube.fbx", aiProcess_Triangulate | aiProcess_JoinIdenticalVertices | aiProcess_GenSmoothNormals);
	
	for (int i = 0; i < scene->mNumMeshes; i++)
	{
		aiMesh* mesh = scene->mMeshes[i];
		int numFaces = mesh->mNumFaces;
		int numVertices = mesh->mNumVertices;
		for (int j = 0; j < mesh->mNumVertices; j++)
		{
			aiVector3D pos = mesh->mVertices[j];
			aiVector3D normal = mesh->mNormals[j];

			verts.push_back(pos.x);
			verts.push_back(pos.y);
			verts.push_back(pos.z);

			norms.push_back(normal.x);
			norms.push_back(normal.y);
			norms.push_back(normal.z);
			uvs.push_back(mesh->mTextureCoords[0][j].x);
			uvs.push_back(mesh->mTextureCoords[0][j].y);

		}

		for (int j = 0; j < mesh->mNumFaces; j++)
		{
			aiFace face = mesh->mFaces[j];
			for (int k = 0; k < face.mNumIndices; k++)
			{
				indices.push_back(face.mIndices[k]);
			}
		}
	}

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * verts.size(), &verts[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, uvo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * uvs.size(), uvs.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ivo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(), indices.data(), GL_STATIC_DRAW);

	const char* vs_shader_code =
		"#version 330 core\n"
		"\n"
		"layout(location = 0) in vec3 vertex_pos;\n"
		"layout(location = 1) in vec2 uv_position;\n"
		"out vec2 UV;\n"
		"uniform mat4 mvp;\n"
		"\n"
		"void main(){\n"
		"gl_Position = mvp * vec4(vertex_pos, 1.0);\n"
		"UV = uv_position;\n"
		"}";

	const char* fs_shader_code =
		"#version 330 core\n"
		"\n"
		"out vec3 out_color;\n"
		"in vec2 UV\n;"
		"uniform sampler2D sam;\n"
		"void main(){\n"
		"out_color = texture2D(sam, UV).rgb;\n"
		"//out_color = vec3(UV, 1.0);\n"
		"//out_color = vec3(1.0, 1.0, 1.0);\n"
		"}";

	GLuint vsShader, fsShader;

	vsShader = glCreateShader(GL_VERTEX_SHADER);
	fsShader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(vsShader, 1, &vs_shader_code, 0);
	glShaderSource(fsShader, 1, &fs_shader_code, 0);

	GLint result;

	glCompileShader(vsShader);
	glCompileShader(fsShader);

	glGetShaderiv(vsShader, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE)
	{
		GLint info_size;
		glGetShaderiv(vsShader, GL_INFO_LOG_LENGTH, &info_size);

		std::vector<GLchar> shader_info_log(info_size + 1);
		glGetShaderInfoLog(vsShader, info_size, NULL, shader_info_log.data());

		std::cout << "Shader compilation error! " << shader_info_log.data() << std::endl;

	}

	glGetShaderiv(fsShader, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE)
	{
		GLint info_size;
		glGetShaderiv(fsShader, GL_INFO_LOG_LENGTH, &info_size);

		std::vector<GLchar> shader_info_log(info_size + 1);
		glGetShaderInfoLog(fsShader, info_size, NULL, shader_info_log.data());

		std::cout << "Shader compilation error! " << shader_info_log.data() << std::endl;

	}

	program = glCreateProgram();

	glAttachShader(program, vsShader);
	glAttachShader(program, fsShader);

	glLinkProgram(program);

	int width = i.GetWidth();
	int height = i.GetHeight();

	GLubyte* texture_bytes = i.GetData();

	glGenTextures(1, &texture);

	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, texture_bytes);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	//texture = HelperFunctions::LoadBMP("./uvtemplate.bmp");

	samplPos = glGetUniformLocation(program, "sam");
	mvpLoc = glGetUniformLocation(program, "mvp");
}

TestCube::TestCube()
{
	i = Image("./uvtemplate.bmp");
	Init(true);
}

TestCube::TestCube(const char * path)
{
	i = Image(path);
	Init(true);

}


TestCube::~TestCube()
{
}

void TestCube::DrawCube(glm::mat4 viewMat, glm::mat4 proj)
{

	glUseProgram(program);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	glUniform1f(samplPos, 0);


	glm::mat4 mvp = proj * viewMat * _transform->GetTransformMatrix();
	glUniformMatrix4fv(mvpLoc, 1, GL_FALSE, &mvp[0][0]);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, uvo);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	//glDrawArrays(GL_TRIANGLES, 0, verts.size());

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ivo);
	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, (void*)0);

	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(0);

}
