#include "Shader.h"



Shader::Shader()
{
}

Shader::Shader(std::string filepath, SHADER_TYPE type)
{

	if (HelperFunctions::FileExists(filepath.c_str()))
	{
		_shaderCode = HelperFunctions::LoadFile(filepath);

		_shaderID = glCreateShader(type);

		const GLchar* _cod = _shaderCode.c_str();

		glShaderSource(_shaderID, 1, &_cod, NULL);

		glCompileShader(_shaderID);

		GLint result;
		glGetShaderiv(_shaderID, GL_COMPILE_STATUS, &result);
		if (result != GL_TRUE)
		{
			GLint len;
			glGetShaderiv(_shaderID, GL_INFO_LOG_LENGTH, &len);

			std::vector<char> buf(len + 1);
			glGetShaderInfoLog(_shaderID, len, NULL, buf.data());

			std::cout << "Error compiling the shader! " << buf.data() << std::endl;
		}


	}
	else
	{
		std::cout << "Could not find shader!";
	}

}


Shader::~Shader()
{
}

GLuint Shader::GetShaderID()
{
	return _shaderID;
}

GLuint Shader::CompileToProgram(Shader * shaders, uint32_t count)
{

	GLuint prog = glCreateProgram();

	for (uint32_t i = 0; i < count; i++)
	{
		glAttachShader(prog, shaders[i].GetShaderID());
	}

	glLinkProgram(prog);

	return prog;
	
}
