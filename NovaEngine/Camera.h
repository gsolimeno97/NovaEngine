#pragma once

#include <iostream>
#include "Transform.h"
#include "IObject.h"

class Camera : IObject
{

private:

	float _fov;
	float _nearPane;
	float _farPane;

	int _renderWidth;
	int _renderHeight;

	Transform* _transform;

public:
	Camera();
	~Camera();


	static Camera mainCamera();

	///SETTERS

	//Sets the actual camera
	void SetMainCamera(Camera* newCamera);

	//Sets field of view of the camera
	void SetFOV(float fov);

	//Sets the near clipping plane
	void SetNearClippingPane(float npane);

	//Sets the far clipping pane
	void SetFarClippingPane(float fpane);

	//Sets the rendering width
	void SetRenderingWidth(int width);

	//Sets the rendering height
	void SetRenderingHeight(int height);

	//Sets the camera's transform
	void SetTransform(Transform* newTrans);

	//Gets instance desc
	std::string ToString() override;

	//Gets camera ID
	uint32_t ObjectID() override;

	///GETTERS

	//Gets the camera's projection matrix
	glm::mat4 GetProjectionMatrix();

	//Gets the camera's view matrix
	glm::mat4 GetViewMatrix();

	//Gets the Camera's FOV (Field of View)
	float GetFOV();

	//Gets near clipping plane
	float GetNearPane();

	//Gets far clipping pane
	float GetFarPane();

	//Gets the aspect ratio
	float GetAspectRatio();

	//Gets the rendering width
	int GetRenderingWidth();

	//Gets the rendering height
	int GetRenderingHeight();

	//Gets the camera transform
	Transform* GetTransform();
};

