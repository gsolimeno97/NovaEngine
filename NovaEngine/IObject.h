#pragma once

#include <string>
#include <sstream>

class IObject
{
public:
	virtual std::string ToString() = 0;
	virtual uint32_t ObjectID() = 0;
};
