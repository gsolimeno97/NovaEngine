#version 330 core

out vec3 out_color;
in vec2 UV;
uniform sampler2D sam;

void main()
{
	out_color = texture2D(sam, UV).rgb;
}