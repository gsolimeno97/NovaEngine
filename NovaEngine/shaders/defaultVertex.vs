#version 330 core

layout(location = 0) in vec3 vertex_pos;
layout(location = 1) in vec2 uv_position;
out vec2 UV;
uniform mat4 mvp;

void main()
{
	gl_Position = mvp * vec4(vertex_pos, 1.0);
	UV = uv_position;
}