#pragma once

#include <GL/glew.h>
#include <FreeImage/FreeImage.h>
#include "IObject.h"

#include "HelperFunctions.h"

class Image : IObject
{
private:
	FIBITMAP* bitmap;
	uint32_t _width;
	uint32_t _height;
	uint32_t _bpp;

	GLuint _texID;

public:
	Image(const char* path);
	Image();

	//Gets image width
	uint32_t GetWidth();

	//Gets image height
	uint32_t GetHeight();

	//Gets image BPP
	uint32_t GetBytesPerPixel();

	//Gets image raw data
	GLubyte* GetData();

	//Gets texture ID 
	GLuint GetTextureID();

	//Gets image desc
	std::string ToString() override;

	//Gets image ID
	uint32_t ObjectID() override;

	~Image();
};

