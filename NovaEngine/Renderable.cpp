#include "Renderable.h"

static uint32_t id;


Renderable::Renderable(std::string filePath)
{

	LoadModel(filePath);

}

Renderable::Renderable(RenderableInfo & info)
{

	LoadModel(info.modelFile);
	SetRenderingProgram( info.renderingProgram);
	SetTexture(info.texture);

}

void Renderable::LoadModel(std::string path)
{
	glGenVertexArrays(1, &_vao);
	glBindVertexArray(_vao);

	glGenBuffers(1, &_vbo);
	glGenBuffers(1, &_uvo);
	glGenBuffers(1, &_ivo);

	const  aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_JoinIdenticalVertices | aiProcess_GenSmoothNormals);
	for (int i = 0; i < scene->mNumMeshes; i++)
	{
		aiMesh* mesh = scene->mMeshes[i];
		int numFaces = mesh->mNumFaces;
		int numVertices = mesh->mNumVertices;
		for (int j = 0; j < mesh->mNumVertices; j++)
		{
			aiVector3D pos = mesh->mVertices[j];
			aiVector3D normal = mesh->mNormals[j];

			verts.push_back(pos.x);
			verts.push_back(pos.y);
			verts.push_back(pos.z);

			norms.push_back(normal.x);
			norms.push_back(normal.y);
			norms.push_back(normal.z);
			uvs.push_back(mesh->mTextureCoords[0][j].x);
			uvs.push_back(mesh->mTextureCoords[0][j].y);

		}

		for (int j = 0; j < mesh->mNumFaces; j++)
		{
			aiFace face = mesh->mFaces[j];
			for (int k = 0; k < face.mNumIndices; k++)
			{
				indices.push_back(face.mIndices[k]);
			}
		}
	}

	glBindBuffer(GL_ARRAY_BUFFER, _vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * verts.size(), &verts[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, _uvo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * uvs.size(), uvs.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ivo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(), indices.data(), GL_STATIC_DRAW);

	id++;
}

void Renderable::Draw(glm::mat4 viewMat, glm::mat4 proj,glm::mat4 modMat)
{
	glUseProgram(_program);
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _texture);
	glUniform1f(samplPos, 0);

	glm::mat4 mvp = proj * viewMat * modMat;
	glUniformMatrix4fv(mvpLoc, 1, GL_FALSE, &mvp[0][0]);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, _vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, _uvo);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ivo);
	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, (void*)0);

	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(0);
}

uint32_t Renderable::ObjectID()
{
	return id;
}

std::string Renderable::ToString()
{
	std::stringstream str;
	str << "Renderable ID: " << id																							<< std::endl
		<< "Vertex Array Object: " << _vao << " Vertex Buffer Object " << _vbo		<< std::endl
		<< " UV Buffer: " << _uvo << " Texture ID: " << _texture									<< std::endl;
	return str.str();
}

void Renderable::SetTexture(GLuint tex)
{
	_texture = tex;
}

void Renderable::SetRenderingProgram(GLuint program)
{

	_program = program;
	samplPos = glGetUniformLocation(_program, "sam");
	mvpLoc = glGetUniformLocation(_program, "mvp");
}

Renderable::~Renderable()
{


}
